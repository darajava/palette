import React, { Component } from 'react';
import './App.css';
import Color from './components/Color';
import AddColor from './components/AddColor';
import Header from './components/Header';
import Footer from './components/Footer';
import colors from './colors.json';
import colorUtil from './utils/color';

const ipc = window.require('electron').ipcRenderer;

class App extends Component {

  constructor() {
    super();

    window.onresize = this.calibrate;
    this.state = {
      colorList: null,
      colorFlat: null,
      sideLength: 0,
      nearest: "",
    };

    if (Math.random() > 0.5) {
      this.state.colorList = this.trim(this.shuffle(colors));
    } else {
      this.state.colorList = this.trim(colors);
    }
    this.state.colorFlat = this.flatten(this.state.colorList)

    console.log(this.state.colorFlat);
  }

  shuffle(a) {
    var j, x, i;
    for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
    return a;
  }

  flatten(colors) {
    let result = {};
    
    colors.map((color) => {
      result[color.name] = color.hex;
    });

    console.log(result);

    return result;
  }

  trim(colors) {
    let start = Math.round(Math.random() * (colors.length - 100));
    let length = Math.round(Math.random() * 100);

    return colors.splice(start, length);
  }

  componentDidMount() {
    this.calibrate();


    ipc.on('eyedropperhex', (event, hex) => {
      console.error(this.state.colorFlat);
      let nearest = colorUtil.findNearest(hex, this.state.colorFlat);

      this.setColor(nearest.value, nearest.name);
      this.setState({
        nearest: nearest.value,
        actual: hex,
      })
    })

    ipc.on('eyedropperclose', () => {
      this.setState({
        nearest: "",
        actual: "",
      })
    })
  }

  calibrate = () => {
    // Add one for the add color
    let numItems = this.state.colorList.length + 6;

    let rectWidth = document.documentElement.clientWidth;
    let rectHeight = document.documentElement.clientHeight - 78;

    let tableRatio = rectWidth / rectHeight;
    let columns = Math.ceil(Math.sqrt(numItems * tableRatio));
    let rows = columns / tableRatio;

    columns = Math.ceil(columns); // the number of columns of squares we will have
    rows = Math.ceil(rows); // the number of rows of squares we will have

    let sideLength = Math.floor(rectWidth / columns); // the size of each square.

    let small = sideLength < 75;

    this.setState({small, sideLength});
  }


  setColor = (hex, name) => {
    this.setState({
      hex,
      name
    });
  }

  generateColourElements = () => {
    let colorStyle = {
      width: this.state.sideLength + "px",
      height: this.state.sideLength + "px",
    }

    let colors = this.state.colorList.map((color) => {
      return (
        <Color
          nearest={this.state.nearest === color.hex}
          key={color.name}
          setColor={this.setColor}
          small={this.state.small}
          hex={color.hex}
          name={color.name}
          colorStyle={{...colorStyle}}
        />
      );
    });

    colors.push(
      <AddColor key="add" small={this.state.small} colorStyle={{...colorStyle}} />
    );

    return colors;
  }


  render() {
    return (
      <div style={{backgroundColor: colorUtil.shadeColor(colorUtil.shadeColor(this.state.hex, 0.25), 0.25)}} className={"window " + (this.state.small ? 'root-small' : '')}>
        <Header hex={this.state.hex} />
        <div className="App" id="colorContainer">
          {this.generateColourElements()}
        </div>
        <Footer hex={this.state.hex} name={this.state.name} actual={this.state.actual}/>
      </div>
    );
  }
}

export default App;
