import React, { Component } from 'react';
import './Header.css';
import {TiPin, TiDelete} from 'react-icons/ti';
import {MdColorize, MdRefresh} from 'react-icons/md';
import color from '../utils/color';

const { remote, screen, ipcRenderer } = window.require('electron');
const { BrowserWindow } = remote;

class Header extends Component {

  constructor() {
    super();

    this.state = {
      onTop: false,
    }
  }

  openColorPicker = () => {
    ipcRenderer.send('openeyedropper', '');
  }

  persistTop = () => {
    BrowserWindow.getAllWindows()[0].setAlwaysOnTop(!BrowserWindow.getAllWindows()[0].isAlwaysOnTop());
    

    this.setState({
      onTop: !this.state.onTop,
    });
  }

  close = () => {
    console.log(BrowserWindow)
    BrowserWindow.getAllWindows()[0].close();
  }

  render() {
    
    return (
      <div className="header" style={{backgroundColor: color.shadeColor(this.props.hex, 0.25)}}>
        <div className="logo">
          pipsqueak
        </div>

        <div>
          <span className="no-drag" onClick={() => document.location = document.location}>
            <div className="button">
              <MdRefresh />
            </div>
          </span>

          <span className="no-drag" onClick={this.openColorPicker}>
            <div className="button">
              <MdColorize />
            </div>
          </span>


          <span className="no-drag" onClick={this.persistTop}>
            {this.state.onTop ?
              <div className="pinned button">
               <TiPin />
              </div>
              :
              <div className="button">
               <TiPin />
              </div>
            }
          </span>

          <span className="no-drag" onClick={this.close}>
            <div className="button">
              <TiDelete />
            </div>
          </span>
        </div>
      </div>
    );
  }
}

export default Header;
