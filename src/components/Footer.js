import React, { Component } from 'react';
import './Header.css';

export default function Footer(props) {

  let actual;
  if (props.actual && props.actual.length) {
    actual = <div className="actual" style={{backgroundColor: props.actual}}>s</div>
  }


  return (
    <div className={"footer"} style={{backgroundColor: props.hex}}>
      <div>
        <div className={"primary-footer" + (props.name === "Copied!" ? '-copied' : '')}>
          {props.name}
        </div>
      </div>
      <div>
        <div className="secondary-footer">
          {props.hex}
        </div>
      </div>
      {actual}
    </div>
  );
}