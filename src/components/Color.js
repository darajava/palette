import React, { Component } from 'react';
import './Color.css';
import colorUtil from '../utils/color';
const {clipboard} = window.require('electron');

class Color extends Component {

  componentDidMount() {
    this.colorRef.addEventListener("mouseover", () => {
      this.props.setColor(this.props.hex, this.props.name);
    });
    this.colorRef.addEventListener("mouseout", () => {
      this.props.setColor();
    });

    this.colorRef.addEventListener("mouseup", () => {
      this.props.setColor(this.props.hex, "Copied!");
      clipboard.writeText(this.props.hex);
    });
  }

  render() {
    let details = (
      <div className="color-name">
        {this.props.name}
      </div>
    );

    let nearestClassName = (this.props.nearest ? " selected " : "")
    let smallClassName = (this.props.small ? " no-padding " : '')


    return (
      <div 
        ref={elem => this.colorRef = elem}
        className={"no-drag color-container " + nearestClassName + smallClassName}
        style={this.props.colorStyle}
      >
        <div
          className="color"
          style={{backgroundColor: this.props.hex}}
        ></div>
        <div className="text">
          {!this.props.small && details}
        </div>
      </div>
    );
  }
}

export default Color;
