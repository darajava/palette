import React, { Component } from 'react';
import './Color.css';
import {TiPlus} from 'react-icons/ti';

class AddColor extends Component {

  render() {
    let details = (
      <div>
        <div className="color-name-noat">
          New...
        </div>
      </div>
    );


    return (
      <div className={"color-container no-drag " + (this.props.small ? 'no-padding' : '')} style={this.props.colorStyle}>
        <div className="color add" style={{backgroundColor: "#ccc"}}><TiPlus /></div>
        {!this.props.small && details}
      </div>
    );
  }
}

export default AddColor;
